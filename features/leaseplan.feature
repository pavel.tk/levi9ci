Feature: Levi9 positive test scenarios
    
    Scenario: As a user, I want to check url of Carlease website

        Given I am on the page
        When I set cookies
        When I check the website url
        Then I should see website url
    
    Scenario: As a user, I want to accept cookies

        Given I see the accept cookies button
        Then I set cookies
        Then I should not see accept cookies button
    
    Scenario: As a user, I want to check popular filter elements

        Given I see the popular filter elements
        When I click on Electic filter
        When I see that button contains text Electric and url changed
        When I click on SUV filter
        When I see that button contains text SUV and url changed
        When I click on Automatic filter
        When I see that button contains text Automatic and url changed
        When I click on Hybrid filter
        When I see that button contains text Hybrid and url changed
        When I click on Petrol filter
        When I see that button contains text Petrol and url changed

    Scenario: As a user, I want to check desktop filters - Popular filters

        Given I see the desktop filters
        When I click on Popular filters
        When I see the expanded popup
        When I click on Best deals in the popup
        When I see button change text to Best deals
        When I click on Configure Yourself in the popup
        When I see button change text to Configure Yourself
        When I click on Delivery van in the popup
        When I see button change text to Delivery van
        When I click on Fast delivery button in the popup
        When I see button change text to Fast delivery

    Scenario: As a user, I want to check desktop filters - Make & Model filters

        Given I see the desktop filters
        When I click on Make and Model filters
        When I see the expanded popup for Make and Model
        When I enter search text Audi in popup
        # When I check the label text changed to Audi
        When I click on Audi checkbox
        Then I see that button contains text Audi and url changed

    Scenario: As a user, I want to check desktop filters - Monthly price

        Given I see the desktop filters
        When I click on Monthly price filters
        When I see the expanded popup for Monthly prices
        When I set monthly price to 200-210 range
        Then I expect filter results to be in 200-210 range

    Scenario: As a user, I want to check desktop filters - monthkm

        Given I see the desktop filters
        When I click on monthkm filters
        When I see the expanded popup for monthkm
        When I set duration and mileage
        Then I expect filter results to be in monthkm range

    Scenario: As a user, I want to check desktop filters - Fuel type

        Given I see the desktop filters
        When I click on Fuel type filter
        When I see the expanded popup for Fuel type
        When I click on CNG in the popup
        When I see button change text to CNG
        When I click on Electric type in the popup
        When I see button change text to Electric type
        When I click on Hybrid type in the popup
        When I see button change text to Hybrid type
        When I click on not defined in the popup
        When I see button change text to Not defined
        When I click on Petrol in the popup
        Then I see button change text to Petrol
    @last
    Scenario: As a user, I want to check desktop filters - more filters
    
        Given I see the desktop filters
        When I click on more filters button
        When I see the popup with filters
        When I click mini mpv checkbox and manual checkbox and click save