import Page from './page';

class LeaseplanPage extends Page {
     /**
     * define elements
     */
     get alert_box() { return $('body > div.optanon-alert-box-wrapper > div.optanon-alert-box-bg > div.optanon-alert-box-body'); }
     get popular_filters() {return $('#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-jrAGrp.Wrapper-sc-1w4pfst.Wrapper-sc-1ub7e89.ieisUx.hvNzUl.jzUGzr > div:nth-child(2)')}
     get electric_filter() {return $('#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-jrAGrp.Wrapper-sc-1w4pfst.Wrapper-sc-1ub7e89.ieisUx.hvNzUl.jzUGzr > div:nth-child(2) > div > div > div:nth-child(1) > a')}
     get SUV_filter_button() {return $('#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-jrAGrp.Wrapper-sc-1w4pfst.Wrapper-sc-1ub7e89.ieisUx.hvNzUl.jzUGzr > div:nth-child(2) > div > div > div:nth-child(2) > a')}
     get Automatic_filter_button() {return $('#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-jrAGrp.Wrapper-sc-1w4pfst.Wrapper-sc-1ub7e89.ieisUx.hvNzUl.jzUGzr > div:nth-child(2) > div > div > div:nth-child(3) > a')}
     get Hybrid_filter_button() {return $('#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-jrAGrp.Wrapper-sc-1w4pfst.Wrapper-sc-1ub7e89.ieisUx.hvNzUl.jzUGzr > div:nth-child(2) > div > div > div:nth-child(4) > a')}
     get Petrol_filter_button() {return $('#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-jrAGrp.Wrapper-sc-1w4pfst.Wrapper-sc-1ub7e89.ieisUx.hvNzUl.jzUGzr > div:nth-child(2) > div > div > div:nth-child(5) > a')}
     get commveh_filter_button() {return $('#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-jrAGrp.Wrapper-sc-1w4pfst.Wrapper-sc-1ub7e89.ieisUx.hvNzUl.jzUGzr > div:nth-child(2) > div > div > div:nth-child(6) > a')}
     get desktop_filters_box() {return $('#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1)')}
     get popular_filter_button() {return $('#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1)')}
     get desktop_filters_popup() {return $('#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div')}
     get best_deals_checkbox() {return $('#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div > div > div:nth-child(1) > div > div > label > span')}
     get vehicle_count() {return $('#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1) > div > div > button > div > div > div.sc-cOajty.MobileGrowGridItem-sc-1ik14oq.SGdCX.iUpVCR > h3')}
     get popular_button_filter() {return $('#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1)')}
     get configure_yourself_checkbox() { return $('#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div > div > div:nth-child(2) > div')}

     /**
      * define or overwrite page methods
      */

}

export default new LeaseplanPage();