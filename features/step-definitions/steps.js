const { Given, When, Then } = require('@cucumber/cucumber');

import LeaseplanPage from '../pageobjects/leaseplan.page';
var assert = require('assert')

Given(/^I am on the page$/, function () {
    browser.url('https://www.leaseplan.com/en-be/business/showroom/')
    var url = browser.getUrl();
    console.log(url);
        // outputs the following:
        // "https://www.leaseplan.com/en-be/business/showroom/"
});

When(/^I check the website url$/, function () {
    expect(browser).toHaveUrl('https://www.leaseplan.com/en-be/business/showroom/')
});

Then(/^I should see website url$/, function () {
    expect(browser).toHaveUrlContaining('showroom')
});

When(/^I set cookies$/, function () {
    browser.setCookies([
        {name: '_dd_s', value: 'rum=1&id=66bbd8e5-1f3a-45a2-9171-e3b07323f56f&created=1624400512627&expire=1624401840206&logs=1', domain: 'www.leaseplan.com'},
        {name: '__extfc', value: '1', domain: 'leaseplan.com'},
        {name: '_hjIncludedInSessionSample', value: '0', domain: 'leaseplan.com'},
        {name: '_hjid', value: 'fff9fdcd-00a8-48a7-930b-328a55a3f20a', domain: 'www.leaseplan.com'},
        {name: '_hjAbsoluteSessionInProgress', value: '1', domain: 'leaseplan.com'},
        {name: 'OptanonAlertBoxClosed', value: '2021-06-22T22:22:02.138Z', domain: 'www.leaseplan.com'},
        {name: '_uetvid', value: '44f732b0d3a811ebbd40fbff207ba065', domain: 'leaseplan.com'},
        {name: 'OptanonConsent', value: 'isIABGlobal=false&datestamp=Wed+Jun+23+2021+01%3A22%3A02+GMT%2B0300+(%D0%92%D0%BE%D1%81%D1%82%D0%BE%D1%87%D0%BD%D0%B0%D1%8F+%D0%95%D0%B2%D1%80%D0%BE%D0%BF%D0%B0%2C+%D0%BB%D0%B5%D1%82%D0%BD%D0%B5%D0%B5+%D0%B2%D1%80%D0%B5%D0%BC%D1%8F)&version=5.13.0&landingPath=NotLandingPage&groups=103%3A1%2C1%3A1%2C2%3A1%2C102%3A1%2C0_55252%3A1%2C3%3A1%2C4%3A1%2C110%3A1%2C111%3A1%2C113%3A1%2C114%3A1%2C118%3A1%2C121%3A1%2C122%3A1%2C123%3A1%2C129%3A1%2C131%3A1%2C134%3A1%2C142%3A1%2C0_21892%3A1%2C101%3A1%2C0_57975%3A1%2C0_57976%3A1%2C0_57978%3A1%2C0_57982%3A1%2C0_57983%3A1%2C0_57987%3A1%2C0_57988%3A1%2C0_57989%3A1%2C0_57990%3A1%2C0_57991%3A1%2C0_57992%3A1%2C0_57993%3A1%2C0_57994%3A1%2C0_57995%3A1%2C0_67525%3A1%2C0_67805%3A1%2C0_99337%3A1%2C0_146649%3A1%2C0_100691%3A1%2C0_146650%3A1%2C0_100685%3A1%2C0_100689%3A1%2C0_146651%3A1%2C0_73434%3A1%2C0_87942%3A1%2C0_100684%3A1%2C0_73408%3A1%2C0_73433%3A1', domain: 'www.leaseplan.com'},
        {name: '_gid', value: 'GA1.2.943197586.1624400519', domain: 'leaseplan.com'},
        {name: 'MUID', value: '366A1CCBA9A6647431C10CF0A8CD656C', domain: 'leaseplan.com'},
        {name: '_uetsid', value: '44f63c10d3a811eb9906f3b668395684', domain: 'leaseplan.com'},
        {name: '_gcl_au', value: '1.1.453420361.1624400523', domain: 'leaseplan.com'},
        {name: '_ga', value: 'GA1.2.1746163597.1624400519', domain: 'leaseplan.com'},
        {name: '_hjTLDTest', value: '1', domain: 'leaseplan.com'},
        {name: 'SSPV', value: 'C7gAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAA', domain: 'leaseplan.com'},
        {name: 'SSRT', value: 'gGLSYAADAA', domain:'www.leaseplan.com', domain: 'leaseplan.com'},
        {name: '_hjFirstSeen', value: '1', domain: 'leaseplan.com'},
        {name: 'SSSC', value: '1.G6976747074649850095.1|221.12105', domain: 'leaseplan.com'},
        {name: 'SSID', value: 'CAC3hR0OAAAAAACAYtJg7wBAAIBi0mABAAAAAAAAAAAAgGLSYAC0Ud0AAAFJLwAAgGLSYAEA', domain: 'leaseplan.com'},
        {name: 'SSLB', value: '1', domain: 'leaseplan.com'},
    ])
    browser.url('https://www.leaseplan.com/en-be/business/showroom/')
});

Given(/^I see the accept cookies button$/, function () {
    LeaseplanPage.alert_box.waitForExist({ timeout: 20000 })
});

Then(/^I should not see accept cookies button$/, function () {
    expect(LeaseplanPage.alert_box).not.toBeDisplayed()
});

Given(/^I see the popular filter elements$/, function () {
    LeaseplanPage.popular_filters.waitForExist({ timeout: 20000 })
});

When(/^I click on Electic filter$/, function () {
    LeaseplanPage.electric_filter.click();
});

Then(/^I see that button contains text Electric and url changed$/, function () {
    expect(browser).toHaveUrl('https://www.leaseplan.com/en-be/business/showroom/?fuelTypes=electric')
});

When(/^I click on SUV filter$/, function () {
    LeaseplanPage.SUV_filter_button.click();
});

When(/^I see that button contains text SUV and url changed$/, function () {
    expect(browser).toHaveUrl('https://www.leaseplan.com/en-be/business/showroom/?bodyTypes=suv')
});

When(/^I click on Automatic filter$/, function () {
    LeaseplanPage.Automatic_filter_button.click();
});

When(/^I see that button contains text Automatic and url changed$/, function () {
    expect(browser).toHaveUrl('https://www.leaseplan.com/en-be/business/showroom/?transmissions=automatic')
});

When(/^I click on Hybrid filter$/, function () {
    LeaseplanPage.Hybrid_filter_button.click();
});

When(/^I see that button contains text Hybrid and url changed$/, function () {
    expect(browser).toHaveUrl('https://www.leaseplan.com/en-be/business/showroom/?fuelTypes=hybrid')
});

When(/^I click on Petrol filter$/, function () {
    LeaseplanPage.Petrol_filter_button.click();
});

When(/^I see that button contains text Petrol and url changed$/, function () {
    expect(browser).toHaveUrl('https://www.leaseplan.com/en-be/business/showroom/?fuelTypes=petrol')
});

When(/^I click on Commercial Vehicle filter$/, function () {
    LeaseplanPage.commveh_filter_button.click();
});

Then(/^I see that click leads to another page in new tab$/, function () {
    console.log(browser.getTitle()) // outputs: "Commercial Vehicles | Leaseplan"
    const handles = browser.getWindowHandles()
    browser.switchToWindow(handles[1])
    console.log(browser.getTitle())
    expect(browser).toHaveUrl('https://www.leaseplan.com/en-be/business/commercial-vehicles/')
    browser.closeWindow()
    browser.switchToWindow(handles[0])
    console.log(browser.getTitle()) // outputs: "Business Lease cars| Leaseplan"
});

Given(/^I see the desktop filters$/, function () {
    LeaseplanPage.desktop_filters_box.waitForExist({ timeout: 20000 })
});

When(/^I click on Popular filters$/, function () {
    browser.pause(5000)
    browser.url('https://www.leaseplan.com/en-be/business/showroom/')
    browser.fullscreenWindow()
    LeaseplanPage.popular_filter_button.waitForExist({ timeout: 20000 })
    LeaseplanPage.popular_filter_button.moveTo()
    LeaseplanPage.popular_filter_button.click()
    browser.pause(5000)
});

When(/^I see the expanded popup$/, function () {
    LeaseplanPage.desktop_filters_popup.waitForExist({ timeout: 20000 })
});

When(/I click on Best deals in the popup$/, function () {
    const best_deals_checkbox = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div > div > div:nth-child(1) > div > div > label > span'
    $(best_deals_checkbox).click()

});

When(/^I click on Best deals checkbox$/, function () {
    const best_deals_checkbox = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div > div > div:nth-child(1) > div'
    const popular_button_filter = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1)'
    const vehicle_count = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1) > div > div > button > div > div > div.sc-cOajty.MobileGrowGridItem-sc-1ik14oq.SGdCX.iUpVCR > h3'
    expect(vehicle_count).toHaveText('5139 to choose from')
    $(best_deals_checkbox).click()
    expect(vehicle_count).toHaveText('23 to choose from')
    expect(popular_button_filter).toHaveText('Best deals')
});

When(/^I see button change text to Best deals$/, function () {
    const popular_button_filter = $('#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1) > div > div > button > div > div > div.sc-cOajty.MobileGrowGridItem-sc-1ik14oq.SGdCX.iUpVCR > h3')
    expect(popular_button_filter).toHaveText('Best deals')
    LeaseplanPage.best_deals_checkbox.click()
});

When(/^I click on Configure Yourself in the popup$/, function () {
    const configure_yourself_checkbox = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div > div > div:nth-child(2) > div'
    const popular_button_filter = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1)'
    $(configure_yourself_checkbox).click()
});

When(/^I see button change text to Configure Yourself$/, function () {
    const configure_yourself_checkbox = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div > div > div:nth-child(2) > div'
    const popular_button_filter = $('#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1) > div > div > button > div > div > div.sc-cOajty.MobileGrowGridItem-sc-1ik14oq.SGdCX.iUpVCR > h3')
    expect(popular_button_filter).toHaveText('Configure yourself')
    $(configure_yourself_checkbox).click()
});

When(/^I click on Delivery van in the popup$/, function () {
    const delivery_van_checkbox = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div > div > div:nth-child(3) > div'
    const popular_button_filter = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1)'
    $(delivery_van_checkbox).click()
});

When(/^I see button change text to Delivery van$/, function () {
    const delivery_van_checkbox = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div > div > div:nth-child(3) > div'
    const popular_button_filter = $('#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1) > div > div > button > div > div > div.sc-cOajty.MobileGrowGridItem-sc-1ik14oq.SGdCX.iUpVCR > h3')
    expect(popular_button_filter).toHaveText('Delivery van')
    $(delivery_van_checkbox).click()
});

When(/^I click on Fast delivery button in the popup$/, function () {
    const fast_delivery_checkbox = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div > div > div:nth-child(4) > div'
    const popular_button_filter = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1)'
    $(fast_delivery_checkbox).click()
});

When(/^I see button change text to Fast delivery$/, function () {
    const fast_delivery_checkbox = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div > div > div:nth-child(4) > div'
    const popular_button_filter = $('#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1) > div > div > button > div > div > div.sc-cOajty.MobileGrowGridItem-sc-1ik14oq.SGdCX.iUpVCR > h3')
    expect(popular_button_filter).toHaveText('Fast delivery')
    $(fast_delivery_checkbox).click()
});

When(/^I click on Make and Model filters$/, function () {
    browser.pause(5000)
    browser.url('https://www.leaseplan.com/en-be/business/showroom/')
    browser.fullscreenWindow()
    const make_model_button = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(2)'
    $(make_model_button).waitForExist({ timeout: 20000 })
    $(make_model_button).moveTo()
    $(make_model_button).click()
    browser.pause(5000)
});

When(/^I see the expanded popup for Make and Model$/, function () {
    const make_model_filters_popup = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(2) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div'
    $(make_model_filters_popup).waitForExist({ timeout: 20000 })
});

When(/^I enter search text Audi in popup$/, function () {
    const popup_search_bar = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(2) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kbAIXj > div > div > div > div.sc-dmlrTW.hOpDBC > input'
    $(popup_search_bar).setValue('Audi')
});

/* When(/^I check the label text changed to Audi$/, function () {
    const popup_filter_label = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(2) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.bUgaxv > div > div > div:nth-child(1) > div.sc-bdfBwQ.bwZCvE > div > div > div > div > label > span > span.sc-gsTCUz.bKzEGB'
    $(popup_filter_label).waitForExist({ timeout: 20000 })
    expect(popup_filter_label).toHaveText('Audi')
}); */

When(/^I click on Audi checkbox$/, function () {
    const popup_filter_label = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(2) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.bUgaxv > div > div > div:nth-child(1) > div.sc-bdfBwQ.bwZCvE > div > div > div > div > label > span > span.sc-gsTCUz.bKzEGB'
    $(popup_filter_label).click()
});

Then(/^I see that button contains text Audi and url changed$/, function () {
    expect(browser).toHaveUrl('https://www.leaseplan.com/en-be/business/showroom/audi/')
});

When(/^I click on Monthly price filters$/, function () {
    browser.pause(10000)
    browser.url('https://www.leaseplan.com/en-be/business/showroom/')
    browser.fullscreenWindow()
    const monthly_price_filter_button = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(3)'
    $(monthly_price_filter_button).waitForExist({ timeout: 20000 })
    $(monthly_price_filter_button).moveTo()
    $(monthly_price_filter_button).click()
    browser.pause(10000)
});

When(/^I see the expanded popup for Monthly prices$/, function () {
    const monthly_prices_filters_popup = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(3) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl'
    $(monthly_prices_filters_popup).waitForExist({ timeout: 20000 })
});

When(/^I set monthly price to 200-210 range$/, function () {
    browser.url('https://www.leaseplan.com/en-be/business/showroom/?monthlyPrice=200,210')
});

Then(/^I expect filter results to be in 200-210 range$/, function () {
    browser.url('https://www.leaseplan.com/en-be/business/showroom/?monthlyPrice=200,210')
});

When(/^I click on monthkm filters$/, function () {
    browser.pause(10000)
    browser.url('https://www.leaseplan.com/en-be/business/showroom/')
    browser.fullscreenWindow()
    const month_km_filter_button = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(4)'
    $(month_km_filter_button).waitForExist({ timeout: 20000 })
    $(month_km_filter_button).moveTo()
    $(month_km_filter_button).click()
    browser.pause(10000)
});

When(/^I see the expanded popup for monthkm$/, function () {
    const month_km_filters_popup = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(4) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div'
    $(month_km_filters_popup).waitForExist({ timeout: 20000 })
});

When(/^I set duration and mileage$/, function () {
    browser.url('https://www.leaseplan.com/en-be/business/showroom/?leaseOption[contractDuration]=36&leaseOption[mileage]=25000')
});

Then(/^I expect filter results to be in monthkm range$/, function () {
    browser.url('https://www.leaseplan.com/en-be/business/showroom/?leaseOption[contractDuration]=36&leaseOption[mileage]=25000')
});

When(/I click on Fuel type filter$/, function () {
    browser.pause(10000)
    browser.url('https://www.leaseplan.com/en-be/business/showroom/')
    browser.fullscreenWindow()
    const fuel_type_button = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(5)'
    $(fuel_type_button).waitForExist({ timeout: 20000 })
    $(fuel_type_button).moveTo()
    $(fuel_type_button).click()
    browser.pause(10000)
});

When(/^I see the expanded popup for Fuel type$/, function () {
    const fuel_type_filters_popup = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(5) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div'
    $(fuel_type_filters_popup).waitForExist({ timeout: 20000 })
});


When(/^I click on CNG in the popup$/, function () {
    const cng_checkbox = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(5) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div > div > div:nth-child(1) > div > div > label > div'
    $(cng_checkbox).click()
});

When(/^I see button change text to CNG$/, function () {
    const cng_checkbox = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(5) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div > div > div:nth-child(1) > div > div > label > div'
    $(cng_checkbox).click()
});


When(/^I click on Diesel type in the popup$/, function () {
    const diesel_checkbox = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(5) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div > div > div:nth-child(2) > div > div > label > div'
    $(diesel_checkbox).click()
});

When(/^I see button change text to Diesel$/, function () {
    const diesel_checkbox = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(5) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div > div > div:nth-child(2) > div > div > label > div'
    $(diesel_checkbox).click()
});

When(/^I click on Electric type in the popup$/, function () {
    const electric_checkbox = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(5) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div > div > div:nth-child(3) > div > div > label > div'
    $(electric_checkbox).click()
});

When(/^I see button change text to Electric type$/, function () {
    const electric_checkbox = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(5) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div > div > div:nth-child(3) > div > div > label > div'
    $(electric_checkbox).click()
});

When(/^I click on Hybrid type in the popup$/, function () {
    const hybrid_checkbox = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(5) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div > div > div:nth-child(4) > div > div > label > div'
    $(hybrid_checkbox).click()
});

When(/^I see button change text to Hybrid type$/, function () {
    const hybrid_checkbox = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(5) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div > div > div:nth-child(4) > div > div > label > div'
    $(hybrid_checkbox).click()
});

When(/^I click on not defined in the popup$/, function () {
    const not_defined_checkbox = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(5) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div > div > div:nth-child(5) > div > div > label > div'
    $(not_defined_checkbox).click()
});

When(/^I see button change text to Not defined$/, function () {
    const not_defined_checkbox = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(5) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div > div > div:nth-child(5) > div > div > label > div'
    $(not_defined_checkbox).click()
});

When(/^I click on Petrol in the popup$/, function () {
    const petrol_checkbox = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(5) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div > div > div:nth-child(6) > div > div > label > div'
    $(petrol_checkbox).click()
});

Then(/^I see button change text to Petrol$/, function () {
    const petrol_checkbox = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(5) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.kvIZyE > div > div > div:nth-child(6) > div > div > label > div'
    $(petrol_checkbox).click()
});

When(/^I click on more filters button$/, function () {
    const more_filters_button = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(2) > div.ClickBoundary-sc-4mcoyq.efNMOi > div > div > button > div'
    $(more_filters_button).click()
});

When(/^I see the popup with filters$/, function () {
    const exit_button = '[data-e2e-modal-top-close-button]'
});

When(/^I click mini mpv checkbox and manual checkbox and click save$/, function () {
    const minimpv_checkbox = '[data-e2e-id="MINI MPV"]'
    const manual_checkbox = '[data-e2e-id="Manual"]'
    const exit_button = '[data-e2e-modal-top-close-button]'
    $(minimpv_checkbox).moveTo()
    $(minimpv_checkbox).click()
    $(manual_checkbox).scrollIntoView()
    $(manual_checkbox).click()
    $(exit_button).moveTo()
    $(exit_button).click()
});

When(/^I put garbage symbol in url$/, function () {
    browser.url('https://www.leaseplan.com/en-be/business/showroom/%')
});

Then(/I see Bad Request error$/, function () {
    const bad_request = 'body > h1'
    let expectedErrorText = 'Bad Request'
    let foundText = $(bad_request).getText()
    assert.strictEqual(expectedErrorText, foundText, 'Bad Request');
});

When(/^I go to page search that not exists$/, function () {
    browser.url('https://www.leaseplan.com/en-be/business/showroom/zzz/')
});

Then(/^I see search error$/, function () {
    expect(browser).toHaveUrl('https://www.leaseplan.com/en-be/business/showroom/zzz/')
    const no_result_text = '#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-jrAGrp.Wrapper-sc-1w4pfst.jrsUaQ.hvNzUl > div > div > div > div > div:nth-child(2)'
    $(no_result_text).waitForExist({ timeout: 20000 })
    let expectedErrorText = '0 to choose from'
    let foundText = $(no_result_text).getText()
    assert.strictEqual(expectedErrorText, foundText, '0 to choose from');
});

When(/^I go to page that not exists$/, function () {
    browser.url('https://www.leaseplan.com/en-be/business/showrooms/')
});

Then(/^I see 404 error$/, function () {
    expect(browser).toHaveUrl('https://www.leaseplan.com/en-be/business/showrooms/')
    const text_404 = '#app > div > div > div > div > div > section > div > div > h1'
    $(text_404).waitForExist({ timeout: 20000 })
    let expectedErrorText = 'Mmm, something went wrong.'
    let foundText = $(text_404).getText()
    assert.strictEqual(expectedErrorText, foundText, 'Mmm, something went wrong.');
});