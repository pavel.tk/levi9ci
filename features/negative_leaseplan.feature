Feature: Levi9 negative test scenarios

    Scenario: As a user, I want to check url of Carlease website

        Given I am on the page
        When I set cookies
        When I check the website url
        Then I should see website url
    
    Scenario: As a user, I want to accept cookies

        Given I see the accept cookies button
        Then I set cookies
        Then I should not see accept cookies button
    
    Scenario: As a user, I want to see Bad Request 400 error

        Given I am on the page
        When I put garbage symbol in url
        Then I see Bad Request error

    Scenario: As a user, I want to see Search error

        Given I am on the page
        When I go to page search that not exists
        Then I see search error

    Scenario: As a user, I want to see Not Found 404 error

        Given I am on the page
        When I go to page that not exists
        Then I see 404 error